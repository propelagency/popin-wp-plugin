<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.propelagency.uk
 * @since      1.0.0
 *
 * @package    Popin
 * @subpackage Popin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Popin
 * @subpackage Popin/includes
 * @author     Luke Clifton <luke@propelagency.uk>
 */
class Popin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
