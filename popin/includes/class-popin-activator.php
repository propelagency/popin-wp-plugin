<?php

/**
 * Fired during plugin activation
 *
 * @link       www.propelagency.uk
 * @since      1.0.0
 *
 * @package    Popin
 * @subpackage Popin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Popin
 * @subpackage Popin/includes
 * @author     Luke Clifton <luke@propelagency.uk>
 */
class Popin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
