<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.propelagency.uk
 * @since             1.0.0
 * @package           Popin
 *
 * @wordpress-plugin
 * Plugin Name:       Popin Video
 * Plugin URI:        https://popin.video
 * Description:      	Installs the Popin.video app widget for WordPress sites
 * Version:           1.0.0
 * Author:            Luke Clifton
 * Author URI:        www.propelagency.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       popin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'POPIN_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-popin-activator.php
 */
function activate_popin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-popin-activator.php';
	Popin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-popin-deactivator.php
 */
function deactivate_popin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-popin-deactivator.php';
	Popin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_popin' );
register_deactivation_hook( __FILE__, 'deactivate_popin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-popin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_popin() {

	$plugin = new Popin();
	$plugin->run();

}
run_popin();
